package com.example.madooding.rockpaperscissors;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Game extends AppCompatActivity implements View.OnClickListener{

    private int mySelection = 0;
    private GamePlay game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Bundle bundle = getIntent().getExtras();
        game = (GamePlay) bundle.getSerializable("game");

        ImageView rockSelectImage = (ImageView) findViewById(R.id.rockSelect);
        ImageView paperSelectImage = (ImageView) findViewById(R.id.paperSelect);
        ImageView scissorsSelectImage = (ImageView) findViewById((R.id.scissorsSelect));

        TextView scoreTextView = (TextView) findViewById(R.id.scoreTextView);
        scoreTextView.setText(game.getScore());

        rockSelectImage.setOnClickListener(this);
        paperSelectImage.setOnClickListener(this);
        scissorsSelectImage.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        clearSelection();
        switch (view.getId()) {
            case R.id.rockSelect : {
                mySelection = GamePlay.ROCK;
                ((ImageView)view).setImageResource(R.drawable.selected_01);
                break;
            }
            case R.id.paperSelect : {
                mySelection = GamePlay.PAPER;
                ((ImageView)view).setImageResource(R.drawable.selected_02);
                break;
            }
            case R.id.scissorsSelect : {
                mySelection = GamePlay.SCISSORS;
                ((ImageView)view).setImageResource(R.drawable.selected_03);
                break;
            }
        }
        doGame(mySelection);
    }

    private void clearSelection(){
        ((ImageView)findViewById(R.id.rockSelect)).setImageResource(R.drawable.select_01);
        ((ImageView)findViewById(R.id.paperSelect)).setImageResource(R.drawable.select_02);
        ((ImageView)findViewById(R.id.scissorsSelect)).setImageResource(R.drawable.select_03);
    }

    private void doGame(int mySelection) {
        int computerHand = game.randomComputerHand();
        int[] imageIdArray = {R.drawable.select_01, R.drawable.select_02, R.drawable.select_03};
        String[] result = {"Lose", "Draw", "Win"};
        ((ImageView)findViewById(R.id.comSelect)).setImageResource(imageIdArray[computerHand - 1]);
        Toast.makeText(getApplicationContext(), result[game.compare(mySelection, computerHand) + 1], Toast.LENGTH_SHORT).show();
        ((TextView)findViewById(R.id.scoreTextView)).setText(game.getScore());
    }
}
