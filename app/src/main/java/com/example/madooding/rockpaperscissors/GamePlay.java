package com.example.madooding.rockpaperscissors;

import java.io.Serializable;

/**
 * Created by madooding on 10/7/2016 AD.
 */
public class GamePlay implements Serializable {
    public static final int ROCK = 1;
    public static final int PAPER = 2;
    public static final int SCISSORS = 3;
    private String name;
    private int win = 0;
    private int draw = 0;
    private int lose = 0;

    public GamePlay(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public int getWin() {
        return win;
    }

    public int getDraw() {
        return draw;
    }

    public int getLose() {
        return lose;
    }


    public int compare(int playerHand, int computerHand){
        if(playerHand == computerHand){
            this.draw++;
            return 0;
        } else if((playerHand == GamePlay.ROCK && computerHand == GamePlay.SCISSORS) || (playerHand == GamePlay.PAPER && computerHand == GamePlay.ROCK) || (playerHand == GamePlay.SCISSORS && computerHand == GamePlay.PAPER)){
            this.win++;
            return 1;
        } else {
            this.lose++;
            return -1;
        }
    }

    public int randomComputerHand(){
        return ((int)(Math.random() * 3)) + 1;
    }

    public String getScore(){
        return this.getName() + " : " + this.getWin() + " Win - " + this.getDraw() + " Draw - " + this.getLose() + " Lose";
    }

}
